import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class FormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: "https://docs.google.com/forms/d/e/1FAIpQLSccp2qeV6L4xoAxcTLgMSQxE2sfFad6AnINiiWGwghRZeX9ng/viewform?usp=sf_link",
      appBar: new AppBar(
        title: new Text("Contactformulier"),
      ),
    );
  }
}
