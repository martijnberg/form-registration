import 'package:flutter/material.dart';

class Values {

// Colors
  static const MaterialColor materialColorOrange = MaterialColor(0xFFFE5000, _materialColorList);
  static const Color colorIncentroOrange = Color(0xFFFE5000);
  static const Color colorIncentroBlue = Color(0xFF041C26);
  static const Color colorIncentroGrey = Color(0xFFEFF0F1);

  static const Map<int, Color> _materialColorList = {
    50: Color.fromRGBO(255, 80, 0, .1),
    100: Color.fromRGBO(255, 80, 0, .2),
    200: Color.fromRGBO(255, 80, 0, .3),
    300: Color.fromRGBO(255, 80, 0, .4),
    400: Color.fromRGBO(255, 80, 0, .5),
    500: Color.fromRGBO(255, 80, 0, .6),
    600: Color.fromRGBO(255, 80, 0, .7),
    700: Color.fromRGBO(255, 80, 0, .8),
    800: Color.fromRGBO(255, 80, 0, .9),
    900: Color.fromRGBO(255, 80, 0, 1),
  };

}